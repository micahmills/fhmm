  function dbreport (){
 $("#loading").toggle();

 air.trace("start");

 //get value of user search input
var dist = $("#distSelect").val();


//create the SQL statement
var selectStmt = new air.SQLStatement();
selectStmt.sqlConnection = conn;

// define the SQL text
sql =
    "SELECT * FROM recipients "+ dist;
selectStmt.text = sql;
	
// empty all tables and divs in report page	
	$("#total").empty();
	$("#savedTotal").empty();
	$(".visualize").remove();


try
{
	air.trace('statement created');
    // execute the statement
    selectStmt.execute();
    // access the result data
    var result = selectStmt.getResult();
    var numTotal = result.data.length;
    var male = female = 0;
    var age1 = age2 = age3 = age4 = age5 = age6 = age7 = age8 = 0;
    var Amp = CP = Crip = Hydro = Mental = Paral = otherType = 0;
    var Accident = birthDefect = Malaria = Polio = otherCause = 0;
    var length1 = length2 = length3 = length4 = length5 = length6 = 0;
    var FWM1 = FWM2 = JAF = PET = otherDevice = unknown = 0;
    var savedY = 0;
    
    air.trace('statement executed');

 	for (i = 0; i < numTotal; i++)
    {
    var row = result.data[i];
 	//determine number by gender
 		if (row.gender == "Male")
 		{ male++; }
  		if (row.gender == "Female")
  		{ female++; }
 	
 	//determine numbers in age brackets
 		if (row.age <= 5)
 		{ age1++;}
 		if (row.age > 5 && row.age<=10)
 		{ age2++; }
 		if (row.age > 10 && row.age<=20)
 		{ age3++; }
 		if (row.age > 20 && row.age<=30)
 		{ age4++; } 		
  		if (row.age > 30 && row.age<=40)
 		{ age5++; }
  		if (row.age > 40 && row.age<=50)
 		{ age6++; }		
 		if (row.age > 50 && row.age<=60)
 		{ age7++; }
 		if (row.age > 60)
 		{ age8++; } 	
 		
 	//determine number by Type of Disability 		
 		if (row.type == "Amputation"){
 		Amp++; }
 		if (row.type == "Cerebral Palsy"){
 		CP++; }
 		if (row.type == "Crippled"){
 		Crip++; }
 		if (row.type == "Hydrocephalus"){
 		Hydro++; }
 		if (row.type == "Mental"){
 		Mental++; }
 		if (row.type == "Paralysis"){
 		Paral++; }
 		if (row.type == "Other"){
 		otherType++; }
 		
 		
 	//determine number by Cause of Disability
 		if (row.cause == "Accident"){
 		Accident++; }
 		if (row.cause == "Birth Defect"){
 		birthDefect++; } 		
 		if (row.cause == "Malaria"){
 		Malaria++; }
 		if (row.cause == "Polio"){
 		Polio++; }
 		if (row.cause == "Other"){
 		otherCause++;}
 
  	//determine number by Length of Disability
 		if (row.length == "From Birth"){
 		length1++; }
 		if (row.length == "0-2 years"){
 		length2++; }
 		if (row.length == "2-5 years"){
 		length3++; }
 		if (row.length == "5-10 years"){
 		length4++; }
 		if (row.length == "10-20 years"){
 		length5++; }
 		if (row.length == "20 or more years"){
 		length6++; }
 		
 	//determine number by Length of Disability
 		if (row.wheelchair == "FWM gen1"){
 		FWM1++; }	
 		if (row.wheelchair == "FWM gen2"){
 		FWM2++; }
 		if (row.wheelchair == "JAF"){
 		JAF++; }
 		if (row.wheelchair == "Pet Cart"){
 		PET++; }
 		if (row.wheelchair == "Other"){
 		otherDevice++; }
 		if (row.wheelchair == "unknown"){
 		unknown++; } 	
 	 //determine number by gender
 		if (row.saved == "Yes")
 		{ savedY++; }
 	}
 	

 }
catch (error)
{
    air.trace("Error message:", error.message);
    air.trace("Details:", error.details);
}

	air.trace('writing to divs');
	//Show the Report Div before drawing graphs   		
		$("#report").show(); 
 	//append the Total number to the Total Div
        $("#total").append("<h2> Total = "+ numTotal +"</h2>");
    //append the Number Saved to the Saved Div   
       $("#savedTotal").append("<h2> Number Saved = "+ savedY +"</h2>");
       	air.trace('saved written');


    //reset cells
    	$(".tablecells").empty();
    //append gender numbers to table	
		$("#Mcell").append(male); //male
		$("#Fcell").append(female); //female
	air.trace("gender cells appended");
		
	//append age numbers to table
		$("#0-5").append(age1);
		$("#6-10").append(age2);
		$("#11-20").append(age3);
		$("#21-30").append(age4);
		$("#31-40").append(age5);
		$("#41-50").append(age6);
		$("#51-60").append(age7);
		$("#60").append(age8);
		
	//append Type of Disability numbers to table
		$("#Amp").append(Amp);
		$("#CP").append(CP);
		$("#Crip").append(Crip);	
		$("#Hydro").append(Hydro);	
		$("#Mental").append(Mental);	
		$("#Paral").append(Paral);	
		$("#otherType").append(otherType);	
		
	//append Cause of Disability numbers to table
		$("#Accident").append(Accident);
		$("#birthDefect").append(birthDefect);
		$("#Malaria").append(Malaria);	
		$("#Polio").append(Polio);	
		$("#otherCause").append(otherCause);	
		
	//append Length of Disability numbers to table
		$("#length1").append(length1);
		$("#length2").append(length2);
		$("#length3").append(length3);
		$("#length4").append(length4);
		$("#length5").append(length5);
		$("#length6").append(length6);

	//append Length of Disability numbers to table
		$("#FWM1").append(FWM1);
		$("#FWM2").append(FWM2);
		$("#JAF").append(JAF);
		$("#PET").append(PET);
		$("#otherDevice").append(otherDevice);
		$("#unknownDevice").append(unknown);
	air.trace('toggle loading');

		
			air.trace('done');

	//visualize.js charts and graphs
				air.trace('drawing gender charts started');
 	 	$('#genderTable').visualize({type: 'pie', height: '200px', width: '200px', pieMargin: 2, appendKey: true}).appendTo('#genderchart'); 

					air.trace('drawing age charts started');
		$('#ageTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#agechart');

				air.trace('drawing type charts started');
  		$('#typeTable').visualize({type: 'bar', width: '450px', parseDirection: 'y'}).appendTo('#typechart');

				air.trace('drawing cause charts started');
   		$('#causeTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#causechart');
				
				air.trace('drawing length charts started');
  		$('#lengthTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#lengthchart');
		
			air.trace('drawing device charts started');
   		$('#deviceTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#devicechart');
	   		air.trace('done');

		$("#loading").toggle();

	
}