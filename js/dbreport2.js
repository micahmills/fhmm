  function dbreport (){

 air.trace("start");
//SQL statement beginning
sqlbegin = "SELECT * FROM recipients";
 
//get value of user search criteria and make sql statement conditions

   	//create SQL condition for distrubution if needed
 	if ($("#distSelect").val() != "*")
 		{
 		var distribution = $("#distSelect").val();
 		var distributionsql="distribution  LIKE '%" +distribution+ "%'";
 		}
   	//create SQL condition for gender if needed
 	if ($("#genderSelect").val() != "*")
 		{
 		var gender = $("#genderSelect").val();
 		var gendersql="gender = '" +gender+ "'";
 		}
 		
//create SQL condition for age if needed
var ageOper = $("#ageOper").val();

if ($("#age").val().length !== 0)
 		{
 		var age = parseInt($("#age").val());
 		var agesql="age " +ageOper+ "'" +age+ "'";
 		}

//create SQL condition for type if needed 		
if ($("#type").val() != "*")
 		{
 		var type = $("#type").val();
  		var typesql="type = '" +type+ "'";
 		}

//create SQL condition for cause if needed 		
if ($("#cause").val() != "*")
 		{
 		var cause = $("#cause").val();
 		var causesql="cause = '" +cause+ "'";
 		}
 
//create SQL condition for length if needed 		
if ($("#length").val() != "*")
 		{
 		var length = $("#length").val();
 		var lengthsql="length = '" +length+ "'";
 		}
 		
//create SQL condition for wheelchair if needed 		
if ($("#wheelchair").val() != "*")
 		{
 		var wheelchair = $("#wheelchair").val();
 		var wheelchairsql="wheelchair = '" +wheelchair+ "'";
 		}

//create SQL condition for saved if needed
if ($("#saved").val() != "*")
 		{
 		var saved = $("#saved").val();
 		var savedsql="saved = '" +saved+ "'";
 		}

//create SQL condition for followup if needed
if ($("#followup").val() != "*")
 		{
 		var followup = $("#followup").val();
 		var followupsql="followup = '" +followup+ "'";
 		}

//create SQL condition for district if needed
if ($("#district").val().length !== 0)
 		{
 		var district = $("#district").val();
 		var districtsql="district LIKE '%" +district+ "%'";
 		}

//create SQL condition for pictureRating if needed 		
var picOper = $("#picOper").val();

if ($("#picRate").val() != "*")
 		{
 		var picRate = parseInt($("#picRate").val());
 		var picsql="(beforeRating" + picOper + "'" + picRate + "' OR afterRating" + picOper + "'" + picRate + "')";
 		}
 		
//create SQL condition for storyRating if needed 		
var storyOper = $("#storyOper").val();		

 if ($("#storyRate").val() != "*")
 		{
 		var storyRate = parseInt($("#storyRate").val());
 		var storysql="storyRating" +storyOper+ "'" +storyRate+ "'";
 		}
//create SQL condition for district if needed
if ($("#name").val().length !== 0)
 		{
 		var name = $("#name").val();
 		var namesql="firstName LIKE '%" +name+ "%'" + "OR secondName LIKE '%" +name+ "%'";
 		}
//create Array from all selected search criteria
air.trace("creating Array");
var sqlArray = [ ];
var a=0;

if (typeof distributionsql != 'undefined')
	{sqlArray[a] = distributionsql; a++;}
	
if (typeof gendersql != 'undefined')
	{sqlArray[a] = gendersql; a++;}
	
if (typeof agesql != 'undefined')
	{sqlArray[a] = agesql; a++;}
	
if (typeof typesql != 'undefined')
	{sqlArray[a] = typesql; a++;}
	
if (typeof causesql != 'undefined')
	{sqlArray[a] = causesql; a++;}
	
if (typeof lengthsql != 'undefined')
	{sqlArray[a] = lengthsql; a++;}
	
if (typeof wheelchairsql != 'undefined')
	{sqlArray[a] = wheelchairsql; a++;}
	
if (typeof savedsql != 'undefined')
	{sqlArray[a] = savedsql; a++;}
	
if (typeof followupsql != 'undefined')
	{sqlArray[a] = followupsql; a++;}
	
if (typeof districtsql != 'undefined')
	{sqlArray[a] = districtsql; a++;}
	
if (typeof picsql != 'undefined')
	{sqlArray[a] = picsql; a++;}
	
if (typeof storysql != 'undefined')
	{sqlArray[a] = storysql; a++;}
	
if (typeof namesql != 'undefined')
	{sqlArray[a] = namesql; a++;}
	
//get array length and 
var x=sqlArray.length;
air.trace ("Array length" + x);

var sqlcond=sqlArray.join(" AND ");

if (sqlcond.length>0) {	
	var sqlstmt = sqlbegin + " WHERE " + sqlcond;
	}
	
if (x===0)
	{var sqlstmt = sqlbegin;}

air.trace (sqlstmt);

//create the SQL statement
var selectStmt = new air.SQLStatement();
selectStmt.sqlConnection = conn;


selectStmt.text = sqlstmt;
	
// empty all tables and divs in report page	
	$("#total").empty();
	$("#savedTotal").empty();
	$(".visualize").remove();
	$("#resultsbody").empty();


try
{
	air.trace('statement created');
    // execute the statement
    selectStmt.execute();
    // access the result data
    var result = selectStmt.getResult();
    var numTotal = result.data.length;
    var male = female = 0;
    var age1 = age2 = age3 = age4 = age5 = age6 = age7 = age8 = 0;
    var Amp = CP = Crip = Hydro = Mental = Paral = otherType = 0;
    var Accident = birthDefect = Malaria = Polio = otherCause = 0;
    var length1 = length2 = length3 = length4 = length5 = length6 = 0;
    var FWM1 = FWM2 = JAF = PET = otherDevice = 0;
    var savedY = 0;
    
    air.trace('statement executed');

 	for (i = 0; i < numTotal; i++)
    {
    var row = result.data[i];
 	//determine number by gender
 		if (row.gender == "Male")
 		{ male++; }
  		if (row.gender == "Female")
  		{ female++; }
 	
 	//determine numbers in age brackets
 		if (row.age <= 5)
 		{ age1++;}
 		if (row.age > 5 && row.age<=10)
 		{ age2++; }
 		if (row.age > 10 && row.age<=20)
 		{ age3++; }
 		if (row.age > 20 && row.age<=30)
 		{ age4++; } 		
  		if (row.age > 30 && row.age<=40)
 		{ age5++; }
  		if (row.age > 40 && row.age<=50)
 		{ age6++; }		
 		if (row.age > 50 && row.age<=60)
 		{ age7++; }
 		if (row.age > 60)
 		{ age8++; } 	
 		
 	//determine number by Type of Disability 		
 		if (row.type == "Amputation"){
 		Amp++; }
 		if (row.type == "Cerebral Palsy"){
 		CP++; }
 		if (row.type == "Crippled"){
 		Crip++; }
 		if (row.type == "Hydrocephalus"){
 		Hydro++; }
 		if (row.type == "Mental"){
 		Mental++; }
 		if (row.type == "Paralysis"){
 		Paral++; }
 		if (row.type == "Other"){
 		otherType++; }
 		
 		
 	//determine number by Cause of Disability
 		if (row.cause == "Accident"){
 		Accident++; }
 		if (row.cause == "Birth Defect"){
 		birthDefect++; } 		
 		if (row.cause == "Malaria"){
 		Malaria++; }
 		if (row.cause == "Polio"){
 		Polio++; }
 		if (row.cause == "Other"){
 		otherCause++;}
 
  	//determine number by Length of Disability
 		if (row.length == "From Birth"){
 		length1++; }
 		if (row.length == "0-2 years"){
 		length2++; }
 		if (row.length == "2-5 years"){
 		length3++; }
 		if (row.length == "5-10 years"){
 		length4++; }
 		if (row.length == "10-20 years"){
 		length5++; }
 		if (row.length == "20 or more years"){
 		length6++; }
 		
 	//determine number by Length of Disability
 		if (row.wheelchair == "FWM gen1"){
 		FWM1++; }	
 		if (row.wheelchair == "FWM gen2"){
 		FWM2++; }
 		if (row.wheelchair == "JAF"){
 		JAF++; }
 		if (row.wheelchair == "Pet Cart"){
 		PET++; }
 		if (row.wheelchair == "Other"){
 		otherDevice++; }
 	
 	 //determine number by gender
 		if (row.saved == "Yes")
 		{ savedY++; }
 		
 		
 		var output = "";
         {
			 output += "<td> <a class='edit editRecord' href='editrecord.html?id=" + row.id + "'></a><br/>" 
			 output += "<a class='edit donorCard' href='donorcard.html?id=" + row.id + "'></a> </td>";
             output += "<td>" + row.firstName + "</td>";
             output += "<td>" + row.secondName + "</td>";
             output += "<td>" + row.gender + "</td>";
             output += "<td>" + row.age + "</td>";
             output += "<td>" + row.type + "</td>";
             output += "<td>" + row.cause + "</td>";
             output += "<td>" + row.length + "</td>";
             output += "<td>" + row.wheelchair + "</td>";
             output += "<td>" + row.district + "</td>";
             output += "<td>" + row.phone + "</td>";
             output += "<td>" + row.saved + "</td>";             
             output += "<td>" + row.followup + "</td>";
             output += "<td>" + row.distribution + "</td>";
             output += "<td class='story'>" + row.story + "</td>";
             output += "<td><img id = before class ='images'  src='" + row.before + "'/></td>";
             output += "<td><img id = after class ='images' src='" + row.after + "'/></td>";
             
             
             //append the the body of the Results Table
        $("#resultsbody").append("<tr>", output, "</tr>")
        }
 	}
 	
 	
 	        $("#searchresults").show(); //Show the previously hidden Results Table on the search page
 	        $('.story').expander(); //truncate story in Table with Read more links 




 }
catch (error)
{
    air.trace("Error message:", error.message);
    air.trace("Details:", error.details);
}

	air.trace('writing to divs');
	//Show the Report Div before drawing graphs   		
		$("#report").show(); 
 	//append the Total number to the Total Div
        $("#total").append("<h2> Total = "+ numTotal +"</h2>");
    //append the Number Saved to the Saved Div   
       $("#savedTotal").append("<h2> Number Saved = "+ savedY +"</h2>");
       	air.trace('saved written');


    //reset cells
    	$(".tablecells").empty();
    //append gender numbers to table	
		$("#Mcell").append(male); //male
		$("#Fcell").append(female); //female
	air.trace("gender cells appended");
		
	//append age numbers to table
		$("#0-5").append(age1);
		$("#6-10").append(age2);
		$("#11-20").append(age3);
		$("#21-30").append(age4);
		$("#31-40").append(age5);
		$("#41-50").append(age6);
		$("#51-60").append(age7);
		$("#60").append(age8);
		
	//append Type of Disability numbers to table
		$("#Amp").append(Amp);
		$("#CP").append(CP);
		$("#Crip").append(Crip);	
		$("#Hydro").append(Hydro);	
		$("#Mental").append(Mental);	
		$("#Paral").append(Paral);	
		$("#otherType").append(otherType);	
		
	//append Cause of Disability numbers to table
		$("#Accident").append(Accident);
		$("#birthDefect").append(birthDefect);
		$("#Malaria").append(Malaria);	
		$("#Polio").append(Polio);	
		$("#otherCause").append(otherCause);	
		
	//append Length of Disability numbers to table
		$("#length1").append(length1);
		$("#length2").append(length2);
		$("#length3").append(length3);
		$("#length4").append(length4);
		$("#length5").append(length5);
		$("#length6").append(length6);

	//append Length of Disability numbers to table
		$("#FWM1").append(FWM1);
		$("#FWM2").append(FWM2);
		$("#JAF").append(JAF);
		$("#PET").append(PET);
		$("#otherDevice").append(otherDevice);
	air.trace('toggle loading');

		
			air.trace('done');

	//visualize.js charts and graphs
				air.trace('drawing gender charts started');
 	 	$('#genderTable').visualize({type: 'pie', height: '200px', width: '200px', pieMargin: 2, appendKey: true}).appendTo('#genderchart'); 

					air.trace('drawing age charts started');
		$('#ageTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#agechart');

				air.trace('drawing type charts started');
  		$('#typeTable').visualize({type: 'bar', width: '450px', parseDirection: 'y'}).appendTo('#typechart');

				air.trace('drawing cause charts started');
   		$('#causeTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#causechart');
				
				air.trace('drawing length charts started');
  		$('#lengthTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#lengthchart');
		
			air.trace('drawing device charts started');
   		$('#deviceTable').visualize({type: 'bar', width: '300px', parseDirection: 'y'}).appendTo('#devicechart');
	   		air.trace('done');
		
}
